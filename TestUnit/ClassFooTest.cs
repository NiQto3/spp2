﻿using System;
using Faker.DTO;
using Faker;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestUnit
{
    [TestClass]
    public class ClassFooTest
    {
        private readonly Faker.Faker _faker = new Faker.Faker();

        [TestMethod]
        public void TestClassFoo()
        {
            var foo = _faker.Create<Foo>();
            Assert.IsNotNull(foo);
        }

        [TestMethod]
        public void TestClassFooConstructor()
        {
            var foo = _faker.Create<Foo>();
            Assert.IsTrue(foo.a != 0);
            Assert.IsTrue(foo.b != 0);
        }

        [TestMethod]
        public void TestClassBoo()
        {
            var foo = _faker.Create<Foo>();
            Assert.IsNotNull(foo.Boo);
        }

        [TestMethod]
        public void TestClassStrProperty()
        {
            var foo = _faker.Create<Foo>();
            Assert.IsNotNull(foo.Str);
        }

    }
}
