﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faker
{
    public class CycleDependencyException : Exception
    {
        public CycleDependencyException(string message) : base(message)
        {

        }

    }
}
